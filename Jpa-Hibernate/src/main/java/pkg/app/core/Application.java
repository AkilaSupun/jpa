package pkg.app.core;

import org.hibernate.Session;

import pkg.app.core.entity.Credential;
import pkg.app.core.entity.User;

public class Application {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		User user1 = new User();

		user1.setFirstName("Akila");
		user1.setLastName("Supun");
		user1.setAddress("akila1address");
		// session.save(user1);

		Credential credential1 = new Credential();
		credential1.setUsername("UserAkila1");
		credential1.setPassword("PassAkila1");

		credential1.setUser(user1);
		user1.setCredential(credential1);

		session.save(user1);

		session.getTransaction().commit();
		session.close();
		HibernateUtil.closeSessionFactory();
	}
}
