package pkg.app.core;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import pkg.app.core.entity.Course;
import pkg.app.core.entity.Credential;
import pkg.app.core.entity.Laptop;
import pkg.app.core.entity.Post;
import pkg.app.core.entity.Shirt;
import pkg.app.core.entity.Teacher;
import pkg.app.core.entity.User;

public class HibernateUtil {
	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		Configuration configuration = new Configuration();
		configuration.addAnnotatedClass(User.class);
		configuration.addAnnotatedClass(Credential.class);
		configuration.addAnnotatedClass(Post.class);
		configuration.addAnnotatedClass(Laptop.class);
		configuration.addAnnotatedClass(Shirt.class);
		configuration.addAnnotatedClass(Course.class);
		configuration.addAnnotatedClass(Teacher.class);

		return configuration.buildSessionFactory(new StandardServiceRegistryBuilder().build());
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory.isClosed()) {
			buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		if (sessionFactory.isOpen()) {
			sessionFactory.close();
		}
	}
}
