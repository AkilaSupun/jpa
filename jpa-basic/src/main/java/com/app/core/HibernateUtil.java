package com.app.core;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {
	private static final EntityManagerFactory entityManagerFactory = buildEntityManager();

	private static EntityManagerFactory buildEntityManager() {
		return Persistence.createEntityManagerFactory("manager1");
	}

	public static EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public static void closeEntityManagerFactory() {
		entityManagerFactory.close();
	}
}
