package com.app.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Account class used to provide all the methods related to the class.
 * 
 * @author Akila
 *
 */
@Entity
@Table(name = "ACCOUNT")
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACCOUNT_ID")
	private Long accountId;

	@Column(name = "HOLDER_NAME")
	private String holderName;

	@Column(name = "BRANCH")
	private String branch;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
	private List<Transaction> transactions = new ArrayList<>();

	/**
	 * <p>
	 * Parameterized Constructor of Account Entity.
	 * </p>
	 * 
	 * @param holderName Name of the Account Holder.
	 * @param branch     Name of the Branch where the account was created.
	 */
	public Account(String holderName, String branch) {
		super();
		this.holderName = holderName;
		this.branch = branch;
	}

	public Long getAccountId() {
		return accountId;
	}

	protected void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

}
