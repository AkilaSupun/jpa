package com.app.core.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTION")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRANSACT_ID")
	private Long transactionId;

	@Column(name = "AMOUNT")
	private BigDecimal amount;

	@ManyToOne
	@JoinColumn(name = "FK_ACCOUNT_ID")
	Account account;

	/**
	 * Parameterized Constructor of the Entity Transaction
	 * 
	 * @param amount  Transaction amount in {@link java.math.BigDecimal}
	 * @param account Account instance which makes the particular Transaction
	 *                {@link com.app.core.entity.Account}
	 */
	public Transaction(BigDecimal amount, Account account) {
		super();
		this.amount = amount;
		this.account = account;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	protected void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
