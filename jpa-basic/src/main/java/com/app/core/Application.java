package com.app.core;

import javax.persistence.EntityManager;

import com.app.core.entity.Account;

public class Application {
	public static void main(String[] args) {
		EntityManager entityManager = HibernateUtil.getEntityManagerFactory().createEntityManager();

		entityManager.getTransaction().begin();

		Account account = new Account("Akila Supun", "Kadawatha");

		entityManager.persist(account);
		entityManager.getTransaction().commit();
		entityManager.close();
		HibernateUtil.closeEntityManagerFactory();
	}
}
